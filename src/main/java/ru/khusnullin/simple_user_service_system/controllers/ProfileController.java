package ru.khusnullin.simple_user_service_system.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.khusnullin.simple_user_service_system.models.User;
import ru.khusnullin.simple_user_service_system.services.ProfileService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/profile")
public class ProfileController {

    private final ProfileService profileService;

    @GetMapping
    public String getProfilePage(@AuthenticationPrincipal(expression = "id")  Long currentUserId, Model model) {
        model.addAttribute("currentUser", profileService.getUser(currentUserId));
        return "profile";
    }

    @PostMapping("/update")
    public String updateProfile(@AuthenticationPrincipal(expression = "id") Long currentUserId,
                                @RequestParam String firstName,
                                @RequestParam String lastName,
                                @RequestParam String email) {
        profileService.updateUser(currentUserId, firstName, lastName, email);
        return "redirect:/profile";
    }


}
