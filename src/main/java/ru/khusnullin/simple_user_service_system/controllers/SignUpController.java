package ru.khusnullin.simple_user_service_system.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.khusnullin.simple_user_service_system.dto.SignUpForm;
import ru.khusnullin.simple_user_service_system.services.ConfirmService;
import ru.khusnullin.simple_user_service_system.services.SignUpService;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class SignUpController {

    private final SignUpService signUpService;

    private final ConfirmService confirmService;

    //возвращает страницу по адресу ".../signup/"
    @GetMapping("/signup")
    public String getSignUpPage(Authentication authentication, Model model) {

        if (authentication != null) {
            return "redirect:/";
        }
        model.addAttribute("signUpForm", new SignUpForm());

        //возвращает шаблон с именем "signup.ftlh"
        return "signup";
    }

    @PostMapping("/signup")
    public String signUp(@Valid SignUpForm form, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            //если имеются ошибки в заполнении полей, то создается атрибут signUpForm для новой страницы и кладется туда та же форма
            model.addAttribute("signUpForm", form);
            return "signup";
        }
        //если все ок, то создается пользователь и перенаправляется на страницу авторизации
        signUpService.signUp(form);
        return "signupsuccess";

    }

    //контроллер для активации аккаунта, возвращает шаблон с результатом
    @GetMapping("/confirm/{uuid}")
    public String confirm(@PathVariable("uuid") String uuid) {
        if (confirmService.confirm(uuid)) {
            return "success_confirm";
        } else {
            return "failed_confirm";
        }
    }
}
