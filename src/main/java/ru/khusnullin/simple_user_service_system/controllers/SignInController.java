package ru.khusnullin.simple_user_service_system.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SignInController {

    @GetMapping("/signin")
    public String signIn(Authentication authentication) {
        if (authentication != null) {
            return "redirect:/";
        }
        return "signin";
    }
}
