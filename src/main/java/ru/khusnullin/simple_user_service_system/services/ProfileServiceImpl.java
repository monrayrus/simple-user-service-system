package ru.khusnullin.simple_user_service_system.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.khusnullin.simple_user_service_system.dto.UserDto;
import ru.khusnullin.simple_user_service_system.models.User;
import ru.khusnullin.simple_user_service_system.repositories.UserRepository;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final UserRepository userRepository;

    @Override
    public UserDto getUser(Long id) {
        return UserDto.from(userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User not found!")));
    }

    @Override
    public void updateUser(Long id, String firstName, String lastName, String email) {
        User user = userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User not found"));

        String oldFirstName = user.getFirstName();
        boolean isFirstNameChanged = (firstName != null && !firstName.equals(oldFirstName));
        if(isFirstNameChanged) {
            user.setFirstName(firstName);
        }

        String oldLastName = user.getLastName();
        boolean isLastNameChanged = (lastName != null && !lastName.equals(oldLastName));
        if(isLastNameChanged) {
            user.setLastName(lastName);
        }

        String oldEmail = user.getEmail();
        boolean isEmailChanged = (email != null && !email.equals(oldEmail));
        if(isEmailChanged) {
            user.setEmail(email);
        }

        userRepository.save(user);

    }


    @Override
    public void deleteUser(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        user.setState(User.State.DELETED);
        userRepository.save(user);
    }

}
