package ru.khusnullin.simple_user_service_system.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.khusnullin.simple_user_service_system.models.User;
import ru.khusnullin.simple_user_service_system.models.UserDetailsImpl;
import ru.khusnullin.simple_user_service_system.repositories.UserRepository;

@RequiredArgsConstructor
@Service("customUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        // достаем пользователя из базы данных и оборачиваем его в объект UserDetailsImpl,  если не находим - выбрасываем UsernameNotFound
        User user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found!"));
        return new UserDetailsImpl(user);
    }

}
