package ru.khusnullin.simple_user_service_system.services;

import ru.khusnullin.simple_user_service_system.dto.UserDto;
import ru.khusnullin.simple_user_service_system.models.User;

public interface ProfileService {

    UserDto getUser(Long id);

    void updateUser(Long id, String firstName, String lastName, String email);

    void deleteUser(Long id);
}
