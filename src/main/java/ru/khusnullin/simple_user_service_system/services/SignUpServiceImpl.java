package ru.khusnullin.simple_user_service_system.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.khusnullin.simple_user_service_system.dto.SignUpForm;
import ru.khusnullin.simple_user_service_system.models.User;
import ru.khusnullin.simple_user_service_system.repositories.UserRepository;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final MailUtil mailUtil;


    @Override
    public void signUp(SignUpForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .role(User.Role.USER)
                .state(User.State.NOT_CONFIRMED)
                .confirmUUID(UUID.randomUUID().toString())
                .build();

        mailUtil.sendMailToConfirm(user.getFirstName(), user.getEmail(), user.getConfirmUUID());

        userRepository.save(user);
    }

}
