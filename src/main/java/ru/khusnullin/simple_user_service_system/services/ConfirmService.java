package ru.khusnullin.simple_user_service_system.services;

public interface ConfirmService {
    boolean confirm(String uuid);
}
