package ru.khusnullin.simple_user_service_system.services;

import ru.khusnullin.simple_user_service_system.dto.SignUpForm;

public interface SignUpService {

    void signUp(SignUpForm form);

}
