package ru.khusnullin.simple_user_service_system.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.khusnullin.simple_user_service_system.models.User;
import ru.khusnullin.simple_user_service_system.repositories.UserRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConfirmServiceImpl implements ConfirmService {

    private final UserRepository userRepository;

    //переводит аккаунт пользователя в статус "подтвержденный", если такой имеется в БД
    @Override
    public boolean confirm(String uuid) {
        Optional<User> user = userRepository.findByConfirmUUID(uuid);
        if (user.isPresent()) {
            User account = user.get();
            account.setState(User.State.CONFIRMED);
            userRepository.save(account);
            return true;
        }
        return false;
    }

}
