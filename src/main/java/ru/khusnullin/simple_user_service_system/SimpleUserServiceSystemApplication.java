package ru.khusnullin.simple_user_service_system;

import freemarker.template.Configuration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.freemarker.SpringTemplateLoader;

@SpringBootApplication
public class SimpleUserServiceSystemApplication {

    //TODO сделать подсветку ошибок при заполнении полей,реализовать изменение данных профиля
    @Bean
    public Configuration forEmailConfig() {
        Configuration configuration = new Configuration(Configuration.VERSION_2_3_22);
        configuration.setTemplateLoader(new SpringTemplateLoader(new DefaultResourceLoader(), "classpath:mails"));
        return configuration;
    }

    //Создание кодировщика пароля, его также можно вынести в отдельный класс
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(SimpleUserServiceSystemApplication.class, args);
    }

}
