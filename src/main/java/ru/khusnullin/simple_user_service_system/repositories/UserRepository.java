package ru.khusnullin.simple_user_service_system.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.khusnullin.simple_user_service_system.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    List<User> findAllByState(User.State state);

    List<User> findByEmailLike(String email);

    Optional<User> findById(Long id);

    Optional<User> findByConfirmUUID(String confirmUUID);

}
