package ru.khusnullin.simple_user_service_system.dto;

import lombok.Data;
import ru.khusnullin.simple_user_service_system.annotations.PasswordValidation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@PasswordValidation
public class SignUpForm {

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @NotEmpty
    private String password;

    @NotEmpty
    private String password2;

    @Email
    @NotEmpty
    private String email;
}
