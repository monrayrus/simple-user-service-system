package ru.khusnullin.simple_user_service_system.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "usr")
public class User implements Serializable {

    private static final long serialVersionUID = 2501529279275655162L;

    public enum Role {
        ADMIN, USER, SUPERUSER
    }

    public enum State {
        CONFIRMED, NOT_CONFIRMED, BANNED, DELETED
    }

    //Используется обертка типа Long, т.к. при создании объекта id может еще не существовать, а значит должен быть Null, примитивный тип данных в этом случае не подходит
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    //аннотация Column позволяет создать колонку в таблице с пользовательским названием
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    //пароль будет храниться в зашифрованном виде
    @Column(name = "hash_password")
    private String hashPassword;

    //будет использоваться для активации аккаунта по e-mail
    private String confirmUUID;

    //вкладывается значение роли пользователя (админ, пользователь и т.д.)
    @Enumerated(value = EnumType.STRING)
    private Role role;

    //вкладывается значение статуса пользователя (активен, удален, забанен и т.д.)
    @Enumerated(value = EnumType.STRING)
    private State state;

    public boolean isAdmin() {
        return role.equals(Role.ADMIN);
    }

}
