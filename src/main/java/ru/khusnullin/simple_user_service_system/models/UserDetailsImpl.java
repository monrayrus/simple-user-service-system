package ru.khusnullin.simple_user_service_system.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserDetailsImpl implements UserDetails {


    private static final long serialVersionUID = -1192510795014288153L;

    private final User user;

    public UserDetailsImpl(User user) {
        this.user = user;
    }

    //то, что может делать пользователь, можно отождествить с ролью
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // берем роль текущего пользователя и кладем в понятный для Spring Security SimpleGrantedAuthority
        SimpleGrantedAuthority auth = new SimpleGrantedAuthority(user.getRole().name());
        return Collections.singleton(auth);
    }

    @Override
    public String getPassword() {
        return user.getHashPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !(user.getState().equals(User.State.BANNED) || user.getState().equals(User.State.DELETED));
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getState().equals(User.State.CONFIRMED);
    }

    public User getUser() {
        return user;
    }

    public Long id() {
        return user.getId();
    }

}
