package ru.khusnullin.simple_user_service_system.annotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PasswordValidator.class)
public @interface PasswordValidation {

    public String message() default "Passwords are not equals";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};
}
