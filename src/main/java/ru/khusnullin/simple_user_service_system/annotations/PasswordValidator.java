package ru.khusnullin.simple_user_service_system.annotations;

import ru.khusnullin.simple_user_service_system.dto.SignUpForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<PasswordValidation, SignUpForm> {

    @Override
    public void initialize(PasswordValidation constraintAnnotation) {
    }

    @Override
    public boolean isValid(SignUpForm form, ConstraintValidatorContext context) {
        String password1 = form.getPassword();
        String password2 = form.getPassword2();
        if(password2 == null || !password1.equals(password2)) {
            return false;
        }

        return true;
    }
}
